
exports.up = function(knex,Promise) {
 return knex.schema.createTable('items', (t)=>{
  t.increments('iid').primary().notNullable();
  t.string('item_name').unique().notNullable();
  t.decimal('price',6,2).notNullable();
 }) 
};

exports.down = function(knex) {
  
};
