
exports.up = function(knex, Promise) {
  return knex.schema.createTable('users', (t)=>{
    t.increments('uid').primary().notNullable();
    t.string('login').unique().notNullable();
    t.text('password').notNullable()
    t.timestamp('registration_date').notNullable().defaultTo(knex.fn.now());
    t.timestamp('last_visit_date').defaultTo(knex.fn.now());
    t.enu('flag', ['active', 'not active'])
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('users')
};
