
exports.up = function(knex, Promise) {
  return knex.schema.createTable('group', (t)=>{
    t.increments('id').notNullable().primary();
    t.string('group').unique().notNullable();
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('group')
};
