
exports.up = function(knex, Promise) {
  return knex.schema.createTable('partners', (t)=>{
    t.increments('pid').notNullable().primary();
    t.string('partner_name').notNullable().unique();
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('partners')
};
