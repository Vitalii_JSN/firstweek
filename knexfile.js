// Update with your config settings.

module.exports = {

  development: {
    client: 'pg',
    connection: {
      host:'localhost',
      user: 'postgres',
      password: '123456',
      database: 'test',
      charset: 'utf8'
    },
    migrations:{
      directory: __dirname + '/knex/migrations'
    },
    seeds:{
      directory: __dirname + '/knex/seeds'
    }
  },





};
